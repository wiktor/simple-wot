# Simple Web of Trust

Builds key validity database from graphs of bindings.

This mimics how GnuPG calculates the Web of Trust from ultimately trusted keys (trust roots) through keys directly signed by the trust roots (that become immediately valid) to other keys in the graph.

The following example uses the library to calculate key validity of a key that is further away in the signature graph:

```rust
use simple_wot::{TrustDb, UserId};

let mut db = TrustDb::new();

let wiktors_key = "6C8857E0D8E8F074";

let chris_key = "F5C83C05D9CEEEEE";
let edwards_key = "9605A1098C63B92A";
let lucas_key = "06EAA066E397832F";
let gios_key = "9EDCC991D9AB457E";

db.add_binding(wiktors_key, UserId::new(chris_key, "Chris Boot <>"));
db.add_binding(wiktors_key, UserId::new(edwards_key, "Edward Betts <>"));
db.add_binding(wiktors_key, UserId::new(lucas_key, "Luca Capello <>"));
db.add_binding(wiktors_key, UserId::new(gios_key, "Giovanni Mascellani <>"));

let steves_key = "587979573442684E";

db.add_binding(lucas_key, UserId::new(steves_key, "Steve McIntyre <>"));
db.add_binding(chris_key, UserId::new(steves_key, "Steve McIntyre <>"));
db.add_binding(gios_key, UserId::new(steves_key, "Steve McIntyre <>"));

let lars_key = "44E17740B8611E9C";

db.add_binding(chris_key, UserId::new(lars_key, "Lars Wirzenius <>"));
db.add_binding(edwards_key, UserId::new(lars_key, "Lars Wirzenius <>"));
db.add_binding(steves_key, UserId::new(lars_key, "Lars Wirzenius <>"));

// no key is valid before adding trust roots
assert!(!db.is_key_trusted(wiktors_key));
db.add_trust_root(wiktors_key);

// trust root is immediately valid
assert!(db.is_key_trusted(wiktors_key));

// keys directly signed by the trust root are immediately valid too
assert!(db.is_key_trusted(edwards_key));
assert!(db.is_key_trusted(lucas_key));
assert!(db.is_key_trusted(gios_key));

// keys not signed directly by the trust root are not valid at this step
assert!(!db.is_key_trusted(steves_key));
assert!(!db.is_key_trusted(lars_key));

db.update();

// since this key is signed by at least 3 valid keys it becomes valid
assert!(db.is_key_trusted(steves_key));

// this one is still invalid as it's signed only by 2 valid keys
assert!(!db.is_key_trusted(lars_key));

db.update();

// and finally this becomes valid as it's signed by a key that became
// valid in previous step
assert!(db.is_key_trusted(lars_key));
```

This library can also be used to consume data in the GnuPG store.
First dump the signature graph from GnuPG:

```sh
$ gpg --list-sigs --with-colons > gpg-wot.txt
```

Then convert it to a one-line-per-signature file format with:

```sh
$ cargo run --bin build-wot < gpg-wot.txt > wot.txt
```

Alternatively it's possible to use Sequoia to parse any keyring (including the GnuPG one):

```sh
gpg --export | cargo run --bin sq-build-wot > sq-wot.txt
```

And finally calculate key validity and print it in a format similar to GnuPG:

```sh
$ cargo run --bin calc-trust 6C8857E0D8E8F074 < wot.txt | head
depth: 0  valid:  108
depth: 1  valid:  51
depth: 2  valid:  33
depth: 3  valid:  2
depth: 4  valid:  1
depth: 5  valid:  0
pub   CC0553468E8A585E
uid           [  full  ] HoratiuHavarneanu <>

pub   79E924EBEDA7F3FD
uid           [  full  ] Alexander John Fisher <>

pub   4D710A407C10ABC9
uid           [marginal] Federico Campoli <>
uid           [  full  ] Federico Campoli <>
uid           [  full  ] Federico Campoli (pgdba.org email) <>
```

The application takes a list of trust roots (ultimately trusted keys) and updates the database until it can make no more progress.

Note that the application works with raw data provided to it and does *not* take into account signatures expirations etc.
