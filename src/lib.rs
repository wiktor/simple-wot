#![doc = include_str!("../README.md")]
#![deny(missing_docs)]

use std::collections::HashMap;
use std::collections::HashSet;
use std::fmt::Display;

/// User ID of a key.
///
/// Represents User ID for a given key.
#[derive(Debug, Hash, Eq, PartialEq, Clone)]
pub struct UserId {
    key: String,
    uid: String,
}

impl UserId {
    /// Constructs a new User ID `uid` of `key`.
    pub fn new<T, U>(key: T, uid: U) -> Self
    where
        T: Into<String>,
        U: Into<String>,
    {
        Self {
            key: key.into(),
            uid: uid.into(),
        }
    }
}

/// Stores bindings between keys and a list of calculated valid keys.
#[derive(Debug, Default)]
pub struct TrustDb {
    from_key_sigs: HashMap<String, Vec<UserId>>,
    valid_keys: HashSet<String>,
}

impl TrustDb {
    /// Constructs a new trust database.
    pub fn new() -> Self {
        Self::default()
    }

    /// Add a link from `from_key` to the User ID `to_userid`.
    pub fn add_binding<T>(&mut self, from_key: T, to_userid: UserId)
    where
        T: Into<String>,
    {
        let entry = self.from_key_sigs.entry(from_key.into()).or_default();
        entry.push(to_userid);
    }

    /// Add a new ultimately trusted key.
    pub fn add_trust_root<T>(&mut self, root: T)
    where
        T: Into<String>,
    {
        let root = root.into();
        if let Some(keys) = self.from_key_sigs.get(&root) {
            self.valid_keys.extend(keys.iter().map(|k| k.key.clone()));
        }
        self.valid_keys.insert(root);
    }

    fn add_valid_key(&mut self, root: String) {
        self.valid_keys.insert(root);
    }

    fn calculate_trust_values(&self) -> HashMap<&UserId, i32> {
        let mut map: HashMap<_, i32> = HashMap::new();
        for root in &self.valid_keys {
            if let Some(key_sigs) = self.from_key_sigs.get(root) {
                for key_sig in key_sigs {
                    let entry = map.entry(key_sig).or_default();
                    *entry += 1;
                }
            }
        }
        map
    }

    /// Returns a number of valid keys in the database.
    pub fn valid_keys_len(&self) -> usize {
        self.valid_keys.len()
    }

    /// Updates the database by doing one step of trust calculations.
    pub fn update(&mut self) {
        let v = self
            .calculate_trust_values()
            .into_iter()
            .filter(|(_, b)| *b >= 3)
            .map(|(k, _)| k.key.clone())
            .collect::<Vec<_>>();
        for key in v {
            self.add_valid_key(key);
        }
    }

    /// Returns `true` if a given `key` is trusted or `false` otherwise.
    pub fn is_key_trusted(&self, key: &str) -> bool {
        self.valid_keys.contains(key)
    }
}

impl Display for TrustDb {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let binding = self.calculate_trust_values();
        let keys = binding
            .iter()
            .map(|(k, _)| k.key.clone())
            .collect::<HashSet<_>>();

        for key in keys {
            writeln!(f, "pub   {}", key)?;
            for (key_uid, trust_value) in binding.iter().filter(|(k, _)| k.key == key) {
                let trust_value = if *trust_value >= 3 {
                    "full"
                } else if *trust_value > 0 {
                    "marginal"
                } else {
                    "unknown"
                };
                writeln!(f, "uid           [{:^8}] {}", trust_value, key_uid.uid)?;
            }

            writeln!(f)?;
        }

        writeln!(f)
    }
}
