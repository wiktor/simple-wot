use std::io::BufRead;
use std::io::BufReader;

fn main() -> std::io::Result<()> {
    let stdin = std::io::stdin();
    let reader = BufReader::new(stdin);
    let mut signed_key: String = "".into();
    let mut signed_uid: String = "".into();
    let mut ignore = false;
    for line in reader.lines() {
        let line = line?;
        let parts = line.split(':').collect::<Vec<_>>();
        match parts[0] {
            "pub" => {
                signed_key = parts[4].into();
                ignore = false;
            }
            "uid" => signed_uid = parts[9].into(),
            "sig" => {
                let signing_key = parts[4];
                if !ignore && signing_key != signed_key {
                    println!("{}\t{}\t{}", signing_key, signed_key, signed_uid);
                }
            }
            "sub" => {
                ignore = true;
            }
            _ => {}
        }
    }
    Ok(())
}
