use std::io::BufRead;
use std::io::BufReader;

use simple_wot::{TrustDb, UserId};
fn main() -> std::io::Result<()> {
    let stdin = std::io::stdin();
    let reader = BufReader::new(stdin);
    let mut db = TrustDb::new();
    for line in reader.lines() {
        let line = line?;
        let parts = line.split('\t').collect::<Vec<_>>();
        db.add_binding(parts[0], UserId::new(parts[1], parts[2]));
    }

    for trust_root in std::env::args().skip(1) {
        db.add_trust_root(trust_root);
    }

    let mut trust_roots = db.valid_keys_len();
    let mut depth = 0;

    // discover new valid keys
    loop {
        db.update();
        let new_trust_roots = db.valid_keys_len();
        eprintln!(
            "depth: {}  valid:  {}",
            depth,
            new_trust_roots - trust_roots
        );
        if trust_roots == new_trust_roots {
            // no progress can be made
            break;
        }
        trust_roots = new_trust_roots;
        depth += 1;
    }

    println!("{}", db);

    Ok(())
}
