use openpgp::cert::CertParser;
use openpgp::parse::Parse;
use sequoia_openpgp as openpgp;

fn main() -> openpgp::Result<()> {
    let stdin = std::io::stdin();
    for cert in CertParser::from_reader(stdin)?.into_iter() {
        let cert = cert?;
        for userid in cert.userids() {
            for certification in userid.certifications() {
                for issuer in certification.issuers() {
                    println!("{}\t{}\t{}", issuer, cert.keyid(), userid.userid());
                }
            }
        }
    }
    Ok(())
}
